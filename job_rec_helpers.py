import numpy as np
import pandas as pd


def fix_recs(df):
    def strings_in_list_to_int(str_list):
        return [int(s) for s in str_list]

    df["recs"] = df["recs"].apply(strings_in_list_to_int)
    return df


def rand_noise_to_df(df, perc_rand, seed=42):
    # TODO: Figure out how to use a seed for this random operation
    n_rows, n_cols = df.shape
    rand_mask = (
        np.random.randint(100 - perc_rand, 100 + perc_rand, size=[n_rows, n_cols]) / 100
    )

    np.fill_diagonal(rand_mask, 1)
    df = df * rand_mask
    df = df.clip(0, 1)

    return df


def create_rand_similarity_df(min_val, max_val, size, index, columns):
    rand_sims = np.random.randint(min_val, max_val, size=size) / 100
    np.fill_diagonal(rand_sims, 1)
    rand_sims = np.clip(rand_sims, 0, 1)
    rand_sims_df = pd.DataFrame(rand_sims)
    rand_sims_df.index = index
    rand_sims_df.columns = columns
    return rand_sims_df


def simi_vals2recs(df, cutoff=0.00, max_recs=100):
    def scores2rank(scr, ids):
        scr_ids = zip(scr, ids)
        z = [x for _, x in sorted(scr_ids, reverse=True) if _ >= cutoff]
        return z[1 : max_recs + 1]

    ids = list(df.index)
    df["score_list"] = df.values.tolist()
    df["recs"] = df["score_list"].apply(scores2rank, args=(ids,))
    df["job_id"] = df.index

    return df[["job_id", "score_list", "recs"]]
